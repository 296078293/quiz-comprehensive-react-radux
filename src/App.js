import React, {Component} from 'react';
import './App.less';
import {BrowserRouter, Link} from "react-router-dom";
import {Route, Switch} from "react-router";
import AddGoods from "./pages/AddGoods/AddGoods";
import {FaHome} from "react-icons/fa";
import HomePage from "./pages/Home/HomePage";
import OrderPage from "./pages/OrderPage/OrderPage";

class App extends Component {

    render() {
        return (
            <div className='App'>
                <BrowserRouter>
                    <header className="header">
                        <Link to={'/'}>
                            <FaHome/>商城
                        </Link>
                        <Link to='/orders'>
                            订单
                        </Link>
                        <Link to='/add-goods'>
                            + 添加商品
                        </Link>

                    </header>
                    <Switch>
                        <Route exact path='/add-goods' component={AddGoods}/>
                        <Route exact path="/" component={HomePage}/>
                        <Route exact path="/orders" component={OrderPage}/>
                    </Switch>
                </BrowserRouter>
                <footer>TW Mail @2018 Create by ForCheng</footer>
            </div>
        );
    }
}


export default App;
