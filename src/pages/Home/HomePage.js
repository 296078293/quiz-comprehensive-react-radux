import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import {getAllGoods} from "../../actions/GoodsAction";
import './HomePage.less'
import AddBtn from "../../components/AddBtn/AddBtn";
class HomePage extends Component {


    componentDidMount() {
        this.props.getAllGoods();
    }

    render() {
        const data = this.props.goods || [];
        return (
            <section className="home-page">
                {data.map(item => (
                    <div><img src={item.url}/>
                        <p>{item.name}</p>
                        <p>单价：{item.price}元/{item.unit}</p>
                    <AddBtn id={item.id}/>
                    </div>
                ))}
            </section>
        );
    }
}

const mapStateToProps = state => ({
    goods: state.goodsReducer.payload
});

const mapDispatchToProps = {
    getAllGoods
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
