import React, {Component} from 'react';
import {addGoods} from "../../actions/GoodsAction";
import connect from "react-redux/es/connect/connect";
import './AddGoods.less';

class AddGoods extends Component {


    constructor(props, context) {
        super(props, context);
        this.state = {
            name: '',
            price: '',
            unit: '',
            url: ''
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.changeName = this.changeName.bind(this);
        this.changePrice = this.changePrice.bind(this);
        this.changeUnit = this.changeUnit.bind(this);
        this.changeUrl = this.changeUrl.bind(this);
    }

    render() {
        return (

            <form className='add-goods' onSubmit={this.handleSubmit}>
                <h1>添加商品</h1>
                <h3><span>* </span>名称：</h3>
                <input type="text" value={this.state.name} onChange={this.changeName}/>
                <h3><span>* </span>价格：</h3>
                <input type="text" value={this.state.price} onChange={this.changePrice}/>
                <h3><span>* </span>单位：</h3>
                <input type="text" value={this.state.unit} onChange={this.changeUnit}/>
                <h3><span>* </span>图片：</h3>
                <input type="text" value={this.state.url} onChange={this.changeUrl}/>
                <button type="submit">提交</button>
            </form>

        );
    }


    handleSubmit(event) {
        console.log(JSON.stringify({name: this.state.name, price: this.state.price}));
        event.preventDefault();
        this.props.addGoods(this.state.name, this.state.price, this.state.unit, this.state.url, this.props.history);
    }

    changeName(event) {
        this.setState({name: event.target.value});
    }

    changePrice(event) {
        this.setState({price: event.target.value});
    }

    changeUnit(event) {
        this.setState({unit: event.target.value});
    }

    changeUrl(event) {
        this.setState({url: event.target.value});
    }
}

const mapStateToProps = state => ({});

const mapDispatchToProps = {
    addGoods
};

export default connect(mapStateToProps, mapDispatchToProps)(AddGoods);
