import React, {Component} from 'react';
import {getAllItems} from "../../actions/ItemsAction";

import connect from "react-redux/es/connect/connect";
import './OrderPage.less'
import DeleteBtn from "../../components/DeleteBtn/DeleteBtn";
class OrderPage extends Component {

    componentDidMount() {
        this.props.getAllItems();
    }

    render() {
        const data = this.props.items;
        if (data.length===0){
            return (<h3>暂无订单，返回商品页面继续购买</h3>);
        }
        return (
            <section className='order-page'>
            <table>
                <tbody>
                <tr>
                <td>名字</td>
                <td>单价</td>
                <td>数量</td>
                <td>单位</td>
                <td>操作</td>
                </tr>
                {data.filter(item=>(item.count!==0)).map((item) => (
                    <tr>
                        <td>{item.goods.name}</td>
                        <td>{item.goods.price}</td>
                        <td>{item.quantity}</td>
                        <td>{item.goods.unit}</td>
                        <td>
                            <DeleteBtn id={item.id} history={this.props.history}/>
                        </td>
                    </tr>
                ))}
                </tbody>
            </table>
            </section>
        );
    }
}

const mapStateToProps = state => ({
    items: state.itemsReducer.items,
});

const mapDispatchToProps = {
    getAllItems
};

export default connect(mapStateToProps, mapDispatchToProps)(OrderPage);
