import {createGoodsFetch, findGoodsFetch} from "../util/fetchUtil";

export const getAllGoods = () => (dispatch) => {
    findGoodsFetch().then(data => {
        dispatch({
            type: 'GET_ALL_GOODS',
            payload: data
        });
    });
};


export const addGoods = (name, price, unit, url, history) => (dispatch) => {
    createGoodsFetch(name, price, unit, url)
        .then(() => history.push('/'))
        .then(() => console.log('SUCCESS'));
};