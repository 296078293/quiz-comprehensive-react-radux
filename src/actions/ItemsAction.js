import {createItemsFetch, deleteItemFetch, findItemsFetch} from "../util/fetchUtil";


export const createItem = (id) => (dispatch) => {
    createItemsFetch(id).then(() => console.log("SUCCESS"));
}

export const getAllItems = () => (dispatch) => {
    findItemsFetch().then(data => {
        dispatch({
            type: 'GET_ALL_ITEMS',
            items: data
        });
    });
};

export const deleteItem = (id) => (dispatch) => {
    deleteItemFetch(id)
        .then(()=>getAllItems()(dispatch))
        .then(() => console.log('SUCCESS'))
        .catch(() => alert("删除失败，稍后再试"));
};