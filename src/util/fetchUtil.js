const uri = 'http://localhost:8080/api';
export const findGoodsFetch = () => {
    return fetch(uri+'/goods/')
        .then(response => response.json());
}

export const createGoodsFetch = (name, price, unit, url) => {
    return fetch(uri+'/goods/', {
        method: 'POST',
        headers: new Headers({
            'Content-Type': 'application/json;charset=UTF-8'
        }),
        body: JSON.stringify({name: name, price: price, unit: unit, url: url})
    });
}

export const findItemsFetch = () => {
    return fetch(uri+'/items/',{method:'GET'})
        .then(response => response.json());
}


export const deleteItemFetch = (id) => {
    return fetch(uri+'/items/'+ id,
        {method: 'DELETE'});
}

export const createItemsFetch = (id) => {
    return fetch(uri+'/items/'+id, {
        method: 'POST'
    });
}