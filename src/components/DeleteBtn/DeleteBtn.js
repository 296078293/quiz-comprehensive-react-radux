import React, {Component} from 'react';
import {deleteItem} from "../../actions/ItemsAction";
import connect from "react-redux/es/connect/connect";

class DeleteBtn extends Component {


    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <button onClick={() => this.deleteItem()}>
                删除
            </button>
        );
    }

    deleteItem() {
        this.props.deleteItem(this.props.id);
    }
}

const mapStateToProps = state => ({
    goods: state.itemsReducer.items
});

const mapDispatchToProps = dispatch => ({
    deleteItem: (id) => dispatch(deleteItem(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(DeleteBtn);