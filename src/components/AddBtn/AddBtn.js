import React, {Component} from 'react';
import {createItem} from "../../actions/ItemsAction";
import connect from "react-redux/es/connect/connect";

class AddBtn extends Component {


    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <button onClick={() => this.createItem(this.props.id)}>
                +
            </button>
        );
    }
    createItem(id) {
        this.props.createItem(id);
    }
}
const mapStateToProps = state => ({
    items: state.itemsReducer.items
});

const mapDispatchToProps = dispatch => ({
    createItem: (id) => dispatch(createItem(id))
});

export default connect(mapStateToProps, mapDispatchToProps)(AddBtn);