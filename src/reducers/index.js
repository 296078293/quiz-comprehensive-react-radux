import {combineReducers} from "redux";
import goodsReducer from "./GoodsReducer";
import itemsReducer from "./ItemReducer";
const reducers = combineReducers({
    goodsReducer:goodsReducer,
    itemsReducer:itemsReducer
});
export default reducers;
