const initState = {
    items: [],
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'GET_ALL_ITEMS':
            return {
                ...state,
                items: action.items
            };
        default:
            return state
    }
};
