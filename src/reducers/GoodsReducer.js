const initState = {
    payload: [],
};

export default (state = initState, action) => {
    switch (action.type) {
        case 'GET_ALL_GOODS':
            return {
                ...state,
                payload: action.payload
            };
        default:
            return state
    }
};
